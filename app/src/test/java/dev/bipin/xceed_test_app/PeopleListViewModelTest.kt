package dev.bipin.xceed_test_app

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import dev.bipin.xceed_test_app.data.api.PeopleApi
import dev.bipin.xceed_test_app.data.model.ApiResponse
import dev.bipin.xceed_test_app.data.model.People
import dev.bipin.xceed_test_app.ui.main.viewmodel.PeopleListViewModel
import dev.bipin.xceed_test_app.utils.SchedulerProvider
import dev.bipin.xceed_test_app.utils.Status
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class PeopleListViewModelTest {

    @Mock
    private lateinit var peopleApi: PeopleApi

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var statusObserver: Observer<Status>

    private lateinit var peopleListViewModel: PeopleListViewModel

    private val schedulerProvider =
        SchedulerProvider(
            Schedulers.trampoline(),
            Schedulers.trampoline()
        )

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        peopleListViewModel = PeopleListViewModel(peopleApi, schedulerProvider)
    }

    @Test
    fun shouldReturnSuccessResponse_WithEmptyPeopleList() {
        Mockito.`when`(peopleApi.getAllPeople())
            .thenReturn(
                Single.just(ApiResponse(people = emptyList()))
            )

        peopleListViewModel.observeStatus().observeForever(statusObserver)
        peopleListViewModel.loadPeople()
        Mockito.verify(peopleApi).getAllPeople()

        val argumentCaptor = ArgumentCaptor.forClass(Status::class.java)
        val expectedLoadingState = Status.Loading
        val expectedSuccessResponseState = Status.Success(ApiResponse(people = emptyList()))

        argumentCaptor.run {
            Mockito.verify(statusObserver, Mockito.times(2)).onChanged(capture())
            val (loadingState, successState) = allValues
            assertEquals(loadingState, expectedLoadingState)
            assertEquals(successState, expectedSuccessResponseState)
        }
    }

    @Test
    fun shouldReturnSuccessResponse_WithFewPeopleList() {
        Mockito.`when`(peopleApi.getAllPeople())
            .thenReturn(
                Single.just(
                    ApiResponse(
                        count = 80,
                        next = "http://swapi.dev/api/people/?page=2",
                        previous = "",
                        people = peopleList
                    )
                )
            )

        peopleListViewModel.observeStatus().observeForever(statusObserver)
        peopleListViewModel.loadPeople()
        Mockito.verify(peopleApi).getAllPeople()

        val argumentCaptor = ArgumentCaptor.forClass(Status::class.java)
        val expectedLoadingState = Status.Loading
        val expectedSuccessResponseState = Status.Success(
            ApiResponse(
                count = 80,
                next = "http://swapi.dev/api/people/?page=2",
                previous = "",
                people = peopleList
            )
        )

        argumentCaptor.run {
            Mockito.verify(statusObserver, Mockito.times(2)).onChanged(capture())
            val (loadingState, successState) = allValues
            assertEquals(loadingState, expectedLoadingState)
            assertEquals(successState, expectedSuccessResponseState)
        }
    }

    @Test
    fun shouldReturnErrorResponse() {
        val exception = RuntimeException("API call failed")
        Mockito.`when`(peopleApi.getAllPeople())
            .thenThrow(exception)

        peopleListViewModel.observeStatus().observeForever(statusObserver)
        peopleListViewModel.loadPeople()
        Mockito.verify(peopleApi).getAllPeople()

        val argumentCaptor = ArgumentCaptor.forClass(Status::class.java)
        val expectedLoadingState = Status.Loading
        val expectedErrorState = Status.Error(
            exception.hashCode(),
            exception.localizedMessage ?: "Something went wrong while fetching people"
        )

        argumentCaptor.run {
            Mockito.verify(statusObserver, Mockito.times(2)).onChanged(capture())
            val (loadingState, errorState) = allValues
            assertEquals(loadingState, expectedLoadingState)
            assertEquals(errorState, expectedErrorState)
        }
    }

    private val peopleList = arrayListOf(
        People(
            name = "Luke Skywalker",
            height = "172",
            mass = "77",
            hairColor = "blond",
            skinColor = "fair",
            eyeColor = "blue",
            birthYear = "19BBY",
            gender = "male"
        ),
        People(
            name = "C-3PO",
            height = "167",
            mass = "75",
            hairColor = "n/a",
            skinColor = "gold",
            eyeColor = "yellow",
            birthYear = "112BBY",
            gender = "n/a"
        ),
        People(
            name = "R2-D2",
            height = "96",
            mass = "32",
            hairColor = "n/a",
            skinColor = "while, blue",
            eyeColor = "red",
            birthYear = "33BBY",
            gender = "n/a"
        )
    )
}
