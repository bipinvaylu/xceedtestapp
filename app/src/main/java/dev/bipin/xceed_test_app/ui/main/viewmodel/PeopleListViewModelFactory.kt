package dev.bipin.xceed_test_app.ui.main.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dev.bipin.xceed_test_app.App
import dev.bipin.xceed_test_app.data.api.PeopleApi
import dev.bipin.xceed_test_app.utils.SchedulerProvider
import java.lang.IllegalArgumentException
import javax.inject.Inject

class PeopleListViewModelFactory @Inject constructor(
    private val peopleApi: PeopleApi,
    private val schedulerProvider: SchedulerProvider,
    app: App
): ViewModelProvider.AndroidViewModelFactory(app) {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(PeopleListViewModel::class.java)) {
            return PeopleListViewModel(peopleApi, schedulerProvider) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}
