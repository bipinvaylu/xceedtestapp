package dev.bipin.xceed_test_app.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import dev.bipin.xceed_test_app.R
import dev.bipin.xceed_test_app.data.model.People
import kotlinx.android.synthetic.main.people_item_layout.view.*

class PeopleAdapter(
    private val peopleList: ArrayList<People>,
    private val onItemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<PeopleAdapter.PeopleViewHolder>(), Filterable {

    private val originalPeopleList = ArrayList<People>()

    init {
        originalPeopleList.clear()
        originalPeopleList.addAll(peopleList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PeopleViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.people_item_layout,
                parent,
                false
            )
        )

    override fun getItemCount() = peopleList.size

    override fun onBindViewHolder(holder: PeopleViewHolder, position: Int) {
        holder.bind(peopleList[position], onItemClickListener)
    }

    fun submitPeopleList(list: List<People>) {
        originalPeopleList.clear()
        originalPeopleList.addAll(list)

        peopleList.clear()
        peopleList.addAll(list)
        notifyDataSetChanged()
    }

    override fun getFilter() = peopleFilter

    class PeopleViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(people: People, onItemClickListener: OnItemClickListener) {
            itemView.textViewName.text = people.name
            itemView.setOnClickListener {
                onItemClickListener.onItemClick(people)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(people: People)
    }

    private val peopleFilter = object : Filter() {
        override fun performFiltering(searchTerm: CharSequence?): FilterResults {
            val filterList = ArrayList<People>()
            if (searchTerm == null || searchTerm.isEmpty()) {
                filterList.addAll(originalPeopleList)
            } else {
                val search: String = searchTerm.toString()
                originalPeopleList.forEach {
                    if (it.isMatching(search)) {
                        filterList.add(it)
                    }
                }
            }
            val filterResults = FilterResults()
            filterResults.values = filterList
            return filterResults
        }

        override fun publishResults(searchTerm: CharSequence?, filterResults: FilterResults) {
            peopleList.clear()
            peopleList.addAll(filterResults.values as Collection<People>)
            notifyDataSetChanged()
        }
    }
}
