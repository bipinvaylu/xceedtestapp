package dev.bipin.xceed_test_app.ui.main.view

import android.os.Bundle
import android.view.*
import dev.bipin.xceed_test_app.R
import dev.bipin.xceed_test_app.data.model.People
import dev.bipin.xceed_test_app.ui.main.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_people_detail.view.*

class PeopleDetailFragment private constructor(
    private val people: People
) : BaseFragment() {

    companion object {
        fun newInstance(people: People) = PeopleDetailFragment(people)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_people_detail, container, false)
        renderPeople(view, people)
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.findItem(R.id.search).isVisible = false
    }

    private fun renderPeople(view: View, people: People) {
        view.textViewName.text = people.name
        view.textViewHeight.text = people.height
        view.textViewMass.text = people.mass
        view.textViewHairColor.text = people.hairColor
        view.textViewSkinColor.text = people.skinColor
        view.textViewEyeColor.text = people.eyeColor
        view.textViewBirthYear.text = people.birthYear
        view.textViewGender.text = people.gender
    }

}
