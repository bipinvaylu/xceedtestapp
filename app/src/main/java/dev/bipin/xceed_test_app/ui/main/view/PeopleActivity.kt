package dev.bipin.xceed_test_app.ui.main.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import dev.bipin.xceed_test_app.R
import dev.bipin.xceed_test_app.data.model.People

class PeopleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_people)

        supportFragmentManager.beginTransaction()
            .replace(R.id.peopleMasterContainer, PeopleListFragment.newInstance())
            .commit()

    }

    fun showPeopleDetail(people: People) {
        supportFragmentManager.beginTransaction()
            .add(R.id.peopleMasterContainer, PeopleDetailFragment.newInstance(people))
            .addToBackStack(PeopleDetailFragment::class.java.name)
            .commit()
    }

}
