package dev.bipin.xceed_test_app.ui.main.view

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dev.bipin.xceed_test_app.R
import dev.bipin.xceed_test_app.data.model.People
import dev.bipin.xceed_test_app.ui.main.adapter.PeopleAdapter
import dev.bipin.xceed_test_app.ui.main.base.BaseFragment
import dev.bipin.xceed_test_app.ui.main.viewmodel.PeopleListViewModel
import dev.bipin.xceed_test_app.ui.main.viewmodel.PeopleListViewModelFactory
import dev.bipin.xceed_test_app.utils.Status
import kotlinx.android.synthetic.main.fragment_people_master.view.*
import javax.inject.Inject

class PeopleListFragment : BaseFragment() {

    companion object {
        fun newInstance() = PeopleListFragment()
    }

    @Inject
    lateinit var peopleListViewModelFactory: PeopleListViewModelFactory

    private var searchMenu: MenuItem? = null

    private val peopleListViewModel: PeopleListViewModel by lazy {
        ViewModelProviders.of(this, peopleListViewModelFactory)
            .get(PeopleListViewModel::class.java)
    }

    private val peopleAdapter: PeopleAdapter =
        PeopleAdapter(arrayListOf(), object : PeopleAdapter.OnItemClickListener {
            override fun onItemClick(people: People) {
                getPeopleActivity().showPeopleDetail(people)
            }
        })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =
            LayoutInflater.from(context).inflate(R.layout.fragment_people_master, container, false)

        setupUi(view)
        observeStatus(view)

        return view
    }


    override fun onStart() {
        super.onStart()
        loadPeople()
    }

    override fun onStop() {
        super.onStop()
        peopleListViewModel.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_people_fragment, menu)
        searchMenu = menu.findItem(R.id.search)
        searchMenu?.isVisible = peopleAdapter.itemCount > 0
        setSearchView((searchMenu?.actionView as SearchView))
    }

    private fun getPeopleActivity() = activity as PeopleActivity

    private fun setupUi(view: View) {
        view.recyclerView.layoutManager = LinearLayoutManager(context)
        view.recyclerView.adapter = peopleAdapter
        view.recyclerView.addItemDecoration(
            DividerItemDecoration(
                view.recyclerView.context,
                (view.recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
    }

    private fun observeStatus(view: View) {
        peopleListViewModel.observeStatus().observe(viewLifecycleOwner,
            Observer { renderStatus(view, it) })
    }

    private fun loadPeople() {
        peopleListViewModel.loadPeople()
    }

    private fun renderStatus(view: View, status: Status) {
        when (status) {
            Status.Loading -> {
                showLoadingIndicator(view)
            }
            is Status.Success -> {
                if (status.apiResponse.people.isNotEmpty()) {
                    updatePeopleList(view, status.apiResponse.people)
                } else {
                    showNoPeopleUI(view)
                }
            }
            is Status.Error -> {
                showError(view, status)
            }
        }
    }

    private fun showLoadingIndicator(view: View) {
        searchMenu?.isVisible = false
        view.progressBar.visibility = View.VISIBLE
        view.recyclerView.visibility = View.GONE
        view.textViewResponse.visibility = View.GONE
    }

    private fun updatePeopleList(view: View, peopleList: List<People>) {
        searchMenu?.isVisible = true
        view.textViewResponse.visibility = View.GONE
        view.progressBar.visibility = View.GONE
        view.recyclerView.visibility = View.VISIBLE
        peopleAdapter.submitPeopleList(peopleList)
    }

    private fun showError(view: View, error: Status.Error) {
        searchMenu?.isVisible = false
        view.textViewResponse.visibility = View.VISIBLE
        view.textViewResponse.text = error.message
        view.recyclerView.visibility = View.GONE
        view.progressBar.visibility = View.GONE
    }

    private fun showNoPeopleUI(view: View) {
        searchMenu?.isVisible = false
        view.textViewResponse.visibility = View.VISIBLE
        view.textViewResponse.text = getString(R.string.no_people_found)
        view.recyclerView.visibility = View.GONE
        view.progressBar.visibility = View.GONE
    }

    private fun setSearchView(searchView: SearchView) {
        searchView.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String?): Boolean {
                    peopleAdapter.filter.filter(newText)
                    return false
                }

                override fun onQueryTextSubmit(query: String?): Boolean {
                    // Do nothing
                    return false
                }
            }
        )
    }
}
