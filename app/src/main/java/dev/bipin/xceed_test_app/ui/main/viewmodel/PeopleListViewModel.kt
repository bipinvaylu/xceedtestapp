package dev.bipin.xceed_test_app.ui.main.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dev.bipin.xceed_test_app.data.api.PeopleApi
import dev.bipin.xceed_test_app.data.model.ApiResponse
import dev.bipin.xceed_test_app.utils.SchedulerProvider
import dev.bipin.xceed_test_app.utils.Status
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

class PeopleListViewModel(
    private val peopleApi: PeopleApi,
    private val schedulerProvider: SchedulerProvider
) : ViewModel() {

    private val status: MutableLiveData<Status> = MutableLiveData()
    private val compositeDisposable = CompositeDisposable()

    private fun getAllPeople(): Single<ApiResponse> {
        return try {
            peopleApi.getAllPeople()
        } catch (e: Exception) {
            Single.error(e)
        }
    }

    fun loadPeople() {
        compositeDisposable.add(getAllPeople()
            .doOnSubscribe { status.postValue(Status.Loading) }
            .compose(schedulerProvider.getSchedulersForSingle())
            .subscribe({
                status.postValue(Status.Success(it))
            }, {
                status.postValue(
                    Status.Error(
                        it.hashCode(),
                        it.localizedMessage ?: "Something went wrong while fetching people"
                    )
                )
            })
        )
    }

    fun observeStatus() = status

    fun dispose() {
        compositeDisposable.dispose()
    }
}
