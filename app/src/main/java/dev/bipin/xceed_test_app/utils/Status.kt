package dev.bipin.xceed_test_app.utils

import dev.bipin.xceed_test_app.data.model.ApiResponse

sealed class Status {
    object Loading: Status()
    data class Success(val apiResponse: ApiResponse): Status()
    data class Error(val code: Int, val message: String): Status()
}
