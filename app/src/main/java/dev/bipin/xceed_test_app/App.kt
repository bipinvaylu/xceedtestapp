package dev.bipin.xceed_test_app

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dev.bipin.xceed_test_app.data.api.ApiConstant
import dev.bipin.xceed_test_app.di.component.DaggerAppComponent
import dev.bipin.xceed_test_app.di.module.AppModule
import dev.bipin.xceed_test_app.di.module.BuildersModule
import dev.bipin.xceed_test_app.di.module.NetworkModule
import javax.inject.Inject

class App : Application(), HasAndroidInjector {

    @Inject lateinit var androidInjector : DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule(ApiConstant.SWAPI_BASE_URL))
            .build()
            .inject(this)

    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

}



