package dev.bipin.xceed_test_app.data.model

import com.google.gson.annotations.SerializedName

data class ApiResponse (
    @SerializedName("count")
    val count: Int = 0 ,

    @SerializedName("next")
    val next: String = "",

    @SerializedName("previous")
    val previous: String = "",

    @SerializedName("results")
    val people: List<People> = ArrayList()
)
