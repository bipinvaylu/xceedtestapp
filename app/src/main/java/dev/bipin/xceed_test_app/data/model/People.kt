package dev.bipin.xceed_test_app.data.model

import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

class People(
    @SerializedName("name")
    val name: String = "",

    @SerializedName("height")
    val height: String = "",

    @SerializedName("mass")
    val mass: String = "",

    @SerializedName("hair_color")
    val hairColor: String = "",

    @SerializedName("skin_color")
    val skinColor: String = "",

    @SerializedName("eye_color")
    val eyeColor: String = "",

    @SerializedName("birth_year")
    val birthYear: String = "",

    @SerializedName("gender")
    val gender: String = "",

    @SerializedName("homeworld")
    val homeworld: String = "",

    @SerializedName("films")
    val films: List<String> = ArrayList(),

    @SerializedName("species")
    val species: List<String> = ArrayList(),

    @SerializedName("vehicles")
    val vehicles: List<String> = ArrayList(),

    @SerializedName("starships")
    val starships: List<String> = ArrayList(),

    @SerializedName("created")
    val created: String = "",

    @SerializedName("edited")
    val edited: String = "",

    @SerializedName("url")
    val url: String = ""
) {

    fun isMatching(searchTerm: String): Boolean {
        return name.toLowerCase(Locale.getDefault())
            .contains(searchTerm.toLowerCase(Locale.getDefault()))
    }
}
