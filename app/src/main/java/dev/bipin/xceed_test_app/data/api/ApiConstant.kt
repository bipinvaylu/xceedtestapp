package dev.bipin.xceed_test_app.data.api


object ApiConstant {
    val SWAPI_BASE_URL: String = "https://swapi.dev/api/"
}
