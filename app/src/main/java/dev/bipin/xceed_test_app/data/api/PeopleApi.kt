package dev.bipin.xceed_test_app.data.api

import dev.bipin.xceed_test_app.data.model.ApiResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET


interface PeopleApi {

    @GET("people/")
    fun getAllPeople(): Single<ApiResponse>

}
