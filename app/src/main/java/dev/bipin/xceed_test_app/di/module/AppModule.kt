package dev.bipin.xceed_test_app.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import dev.bipin.xceed_test_app.App
import dev.bipin.xceed_test_app.ui.main.viewmodel.PeopleListViewModelFactory
import dev.bipin.xceed_test_app.utils.SchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideApp(): App = app

    @Provides
    @Singleton
    fun providePeopleListViewModelFactory(
        factoryPeopleList: PeopleListViewModelFactory
    ): ViewModelProvider.AndroidViewModelFactory = factoryPeopleList

    @Provides
    @Singleton
    fun provideSchedulerProvider() =
        SchedulerProvider(Schedulers.io(), AndroidSchedulers.mainThread())
}
