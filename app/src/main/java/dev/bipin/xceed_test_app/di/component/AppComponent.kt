package dev.bipin.xceed_test_app.di.component

import dagger.Component
import dagger.android.AndroidInjectionModule
import dev.bipin.xceed_test_app.App
import dev.bipin.xceed_test_app.di.module.AppModule
import dev.bipin.xceed_test_app.di.module.BuildersModule
import dev.bipin.xceed_test_app.di.module.NetworkModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        BuildersModule::class,
        NetworkModule::class
    ]
)
interface AppComponent {

//    fun inject(peopleApiRepository: PeopleApiRepository)
//    fun inject(peopleViewModel: PeopleViewModel)
//    fun inject(viewModelFactory: ViewModelFactory)
    fun inject(app: App)
}
