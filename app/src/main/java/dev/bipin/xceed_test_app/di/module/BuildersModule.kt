package dev.bipin.xceed_test_app.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.bipin.xceed_test_app.ui.main.view.PeopleActivity
import dev.bipin.xceed_test_app.ui.main.view.PeopleDetailFragment
import dev.bipin.xceed_test_app.ui.main.view.PeopleListFragment

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun contributePeopleActivity(): PeopleActivity

    @ContributesAndroidInjector
    abstract fun contributePeopleListFragment(): PeopleListFragment

    @ContributesAndroidInjector
    abstract fun contributePeopleDetailFragment(): PeopleDetailFragment

}
